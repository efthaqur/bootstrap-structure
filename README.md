

Bootstrap Basic Structure
=======

> Written by [Efthaqur Alam](http://efthaqur.mynetwall.info)

**Basic Structure**
 This Structure is precomplied as getbootstrap.com
 I created this as anyone can start coding without linking css or js!
 A index.html document is created in directory, make a copy of it and start coding. 
 **Note That** Additional Bootstrap assets need to be linked if required, I am still updating this.
 Thank you

**Directory Map**
   

    ├── css/
        │   ├── bootstrap.css
        │   ├── bootstrap.css.map
        │   ├── bootstrap.min.css
        │   ├── bootstrap.min.css.map
        │   ├── bootstrap-theme.css
        │   ├── bootstrap-theme.css.map
        │   ├── bootstrap-theme.min.css
        │   └── bootstrap-theme.min.css.map
        ├── js/
        │   ├── bootstrap.js
        │   └── bootstrap.min.js
        └── fonts/
            ├── glyphicons-halflings-regular.eot
            ├── glyphicons-halflings-regular.svg
            ├── glyphicons-halflings-regular.ttf
            ├── glyphicons-halflings-regular.woff
            └── glyphicons-halflings-regular.woff2

